var formidable = require('formidable'),
	fs = require('fs'),
	slugize = require('slugize-component'),
	express = require('express'),
	app = express(),
	server = require('http').createServer(app),
	io = require('socket.io').listen(server, {log: false}),
	walk = require('walk'),
	file = './log/'+ todayDate() +'.txt',
	usernames = {},
	filelist = {
		'timestamp' : 0,
		'files' : []
	};

app.configure(function(){
	app.use(express.static(__dirname + '/static'));
	app.use(express.bodyParser({uploadDir:'./static/upload'}));
	app.set('view engine', 'jade');
	app.set('views', __dirname + '/views');
});

app.get('/', function(req, res){
	var content = [];
	var walker = walk.walk('./static/upload', {followLinks: false});
	 walker.on('file', function(root, stat, next){
		content.push({
			'name': stat.name
		});
		next();
	});
	walker.on('end', function() {
		res.render('index', {
			content: content
		});
	});
})

// app.post('/upload', function(req, res){
// 	var form = new formidable.IncomingForm()
// 	form.parse(req, function(err, fields, files){
// 		for(var i in files) {
// 			// filelist.files.push("/static/upload/" + files[i].name);
// 			req.form.uploadDir = "/static/upload/";
// 			console.log(files[i].name + " is being uploaded");
// 			name = getFilename(files[i].name);
// 			extension = getExtension(files[i].name);
// 			filename = slugize(name);
// 			console.log(files[i].name + " saved as " + filename  + extension);
// 			filelist.timestamp = (new Date()).toLocaleTimeString();
// 			fs.rename(files[i].path, __dirname + "/static/upload/" + filename + extension, function(err) {
// 				if (err) {
// 					fs.unlink(__dirname + "/static/upload/" + filename + extension);
// 					fs.rename(files[i].path, __dirname + "/static/upload/" + filename + extension);
// 				}
// 			});
// 		}
// 	})
// });

app.post('/upload', function(req, res, next){
	console.log(req.files.file.name + " is being uploaded");
	name = getFilename(req.files.file.name);
	extension = getExtension(req.files.file.name);
	filename = slugize(name);
	console.log(req.files.file.name + " saved as " + filename  + extension);
	filelist.timestamp = (new Date()).toLocaleTimeString();
	tmp_path = req.files.file.path;
	target_path = './static/upload/' + filename + extension;
	fs.rename(tmp_path, target_path, function(err){
		if (err) throw err;
		fs.unlink(tmp_path, function(){
			if (err) throw err;
			
		})
	})
})

app.get('/update', function(req, res){
	if (req.headers.accept && req.headers.accept == 'text/event-stream') {
		res.writeHead(200, {
			'Content-Type': 'text/event-stream',
			'Cache-Control': 'no-cache',
			'Connection': 'keep-alive'
		});
		sendSSE(req, res);
	}
});

function todayDate() {
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1;
	var yyyy = today.getFullYear();
	if(dd<10){dd='0'+dd} if(mm<10){mm='0'+mm} newdate = mm+'-'+dd+'-'+yyyy;
	return newdate;
}

fs.exists(file, function(exists) {
	if (!exists) {
		fs.open(file, 'w');
	}
})

function logger(data) {
	var today = todayDate();
	var log = fs.createWriteStream(file, {flags: 'a'});
	log.write(data);
}

function logReader() {
	var today = todayDate();
	var msg = new Array();
	var msg = fs.readFileSync(file).toString().split("\n");
	return msg;
}

function sendSSE(req, res) {
	var id = (new Date()).toLocaleTimeString();
	constructSSE(res, id);
}

function constructSSE(res, id) {
		if(filelist.files.length) {
			res.write('id: ' + id + '\n');
			res.write('data: ' + JSON.stringify(filelist) + '\n\n');
		}
		setTimeout(function() {
			constructSSE(res, id);
		}, 5000);
}

function getExtension(filename) {
	var i = filename.lastIndexOf('.');
	return (i < 0) ? '' : filename.substr(i);
}

function getFilename(filename) {
	var f = filename.lastIndexOf('.');
	return (f < 0) ? '' : filename.substring(0, f);
}

io.sockets.on('connection', function (socket) {
	socket.on('sendchat', function (data) {
		io.sockets.emit('updatechat', socket.username, data);
		console.log(socket.username + ' : '+ data);
		logger(socket.username + ' : ' + data + '\n');
	});
	socket.on('adduser', function(username) {
		socket.username = username;
		usernames[username] = username;
		data = logReader();
		socket.emit('updatechat', 'SERVER', 'you have connected');
		socket.emit('backlog', 'SERVER', data);
		socket.broadcast.emit('updatechat', 'SERVER', username + ' has connected');
		socket.broadcast.emit('notify', 'SERVER', username + ' has connected');
		console.log(username + ' connected');
		logger(username + ' : conected\n');
	});
	socket.on('uploaded', function(username) {
		socket.username = username;
		socket.emit('updatechat', 'SERVER', 'your files is uploaded');
		socket.broadcast.emit('updatechat', 'SERVER', username + ' has uploaded a file');
		socket.broadcast.emit('notify', 'SERVER', username + ' has uploaded a file');
		io.sockets.emit('refresh', 'SERVER');
		console.log(username +' uploaded a file');
		logger(username + ' uploaded a file\n');
	});
	socket.on('disconnect', function(){
		delete usernames[socket.username];
		socket.broadcast.emit('updatechat', 'SERVER', socket.username + ' has disconnected');
		console.log(socket.username + ' has been disconnected');
		logger(socket.username + ' has been disconnected\n')
	});
});

// console.log("Listening");
server.listen(8000, function() {
	console.log("drupload running on port 8000");
});
