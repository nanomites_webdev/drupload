#Drupload
---------

![screenshot](http://f.cl.ly/items/2Z2P0s0P3R1J323p1d3T/Screen%20Shot%202012-09-28%20at%206.32.13%20PM.png)

Drupload is a web app capable of uploading all files via drag - and drop.  
built on Node.js and Express Framework.  

#How to Run
-----------

1. `git clone` this repository.
2. `cd` to repo.
3. run `npm install` to install required modules/dependencies.
4. run `node index.js` to run.
5. access the app at [localhost:8000](http://localhost:8000).
6. profit !

files uploaded will be placed at 'static/upload', and will be visible at `STDOUT`.  
maybe still some sort of hacks, but damn, it's just work.

#Use Case
---------

1. bikin dropbox kecil kecilan 


#Changelog
----------

* **0.0.5**  
Drupload is now fully cross-platform.
tested on Windows 8 with nodejs 0.8.21

* **0.0.4**  
add non-UNIX compatibility - please test if you're on windows box! 
tested on XP sp3 with nodejs 0.8.1

* **0.0.3**  
add ability to logs chat  

* **0.0.2**  
add socket.io to dependency list  
add jquery to simplify some things  
add a simple chat server with upload notifications
minor layout changes

* **0.0.1**  
init project based on andrei oprea's dragdrop
add support for express.js version 3
