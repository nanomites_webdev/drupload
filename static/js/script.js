
var socket = io.connect();
var output = $('#dropFiles');
var itemlist = $('#itemlist');
var timestamp=0;
var username;

var images = ['/img/1.jpg','/img/2.jpg','/img/3.jpg','/img/4.jpg','/img/5.jpg'];
var image = images[Math.floor(Math.random()*images.length)];

// dropbox = document.getElementById("dropzone");
// dropbox.addEventListener("dragenter", dragenter, false);
// dropbox.addEventListener("dragover", dragover, false);
// dropbox.addEventListener("drop", drop, false);

socket.on('connect', function() {
	username = prompt("Username chat");
	socket.emit('adduser', username);
});

socket.on('updatechat', function (username, data) {
	$('#conversation').append('<b>'+username + ':</b> ' + data + '<br>');
	$('#conversation').animate({scrollTop: $('#conversation').prop('scrollHeight')}, "fast");
	$('#itemlist').load("/ #itemlist");
});

socket.on('backlog', function (username, data) {
	// msg = data.toString().split(",");
	for(i in data) {
		$('#conversation').append(data[i]+'<br>');
	}
	$('#conversation').append('<center>--- end log ---</center><br>');
	$('#conversation').animate({scrollTop: $('#conversation').prop('scrollHeight')}, "fast");
});

socket.on('notify', function (username, data) {
	$.meow({
		message: data,
		icon: image
	});
	$('#itemlist').load("/ #itemlist");
});

socket.on('refresh', function() {
	$('#itemlist').load("/ #itemlist");
});

var fileUp = $("#dropzone").dropzone();
fileUp.on("drop", function() {
	socket.emit('uploaded', username);
});

var appendItems = function(file) {
	var content = '';
	if(file.length>1) {
		for (var i = 0; i < file.length; i++) {
			var item = file[i].split('/');
			if(!checkForFile(item[item.length-1]))
				content += '<li class="new"><a href=/upload/' + file[i] + ' target="_blank">'+ item[item.length-1] +'</a></li>';
		}
	} else {
		var item = file[0].split('/');
		content += '<li class="new"><a href=/upload/' + file + ' target="_blank"></a></li>';
	}
	itemlist.innerHTML += content;
}

function refresh_list() {
	$('#itemlist').load("/ #itemlist");
}

// on load of page
$(function(){
	$('#datasend').click( function() {
		var message = $('#data').val();
		$('#data').val('');
		socket.emit('sendchat', message);
	});

	$('#chatTgl').click( function() {
		$('#chatbox').toggle();
	});

	$('#data').keypress(function(e) {
		if(e.which == 13) {
			$(this).blur();
			$('#datasend').focus().click();
			$('#data').focus();
		}
	});
	setInterval(refresh_list, 30000);
});

$(document).keyup(function(e) {
	if (e.keyCode == 27) {
		$('#chatbox').toggle();
	}
})